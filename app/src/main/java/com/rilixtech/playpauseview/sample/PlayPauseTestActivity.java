package com.rilixtech.playpauseview.sample;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.rilixtech.widget.playpauseview.PlayPauseView;


public class PlayPauseTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_pause_test);
        final PlayPauseView view = findViewById(R.id.play_pause_view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.toggle();
            }
        });

        view.setPlayBackgroundColor(Color.BLUE);
        view.setPauseBackgroundColor(Color.GREEN);
    }
}
